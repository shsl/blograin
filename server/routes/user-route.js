'use strict';

import {userController} from '../controllers/index';
import {Wrapper} from '../helpers/index';
import {authController} from '../controllers/index';

const isAuth = authController.isAuth;
module.exports = (app, router) => {

    // router.route('/user').get([], Wrapper(userController.getAll));
    router.route('/user').get([isAuth], Wrapper(userController.get));
    router.route('/user').put([isAuth], Wrapper(userController.update));
};


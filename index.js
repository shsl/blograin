'use strict';

import Express from 'express';
import BodyParser from 'body-parser';
import Cors from 'cors';
import Morgan from 'morgan';
import FS from 'fs-extra';
import Helmet from 'helmet';
import ExpressValidator from 'express-validator';

const app = Express();
const router = Express.Router();

app.use((req, res, next) => {
    if (typeof(req.headers['content-type']) === 'undefined') {
        req.headers['content-type'] = "application/json";
    }
    next();
})
    .use(Helmet())
    .use(Cors())
    .use(BodyParser.json())
    .use(BodyParser.urlencoded({extended: true}))
    .use(ExpressValidator())
    .use(Helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            styleSrc: ["'self'", "'unsafe-inline'"]
        }
    }))
    .use(Helmet.hsts({
        maxAge: 31536000,
        includeSubDomains: true
    }))
    .use(Helmet.frameguard({action: 'sameorigin'}))
    .use(Helmet.xssFilter({setOnOldIE: true}))
    .use(Helmet.noSniff())
    .use(Helmet.referrerPolicy({policy: 'same-origin'}))
    .disable('x-powered-by');

app
    .use(Cors())
    .use(BodyParser.json())
    .use(BodyParser.urlencoded({extended: true}))
    .use('/api', router)
    .use(Morgan('combined'));

const routePath = `${__dirname}/server/routes/`;
FS
    .readdirSync(routePath)
    .forEach((fileName) => {
        require(`${routePath}${fileName}`)(app, router);
    });

module.exports = app;
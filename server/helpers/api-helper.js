'use strict';

import Request from 'request';

export default class ApiHelper {

	constructor(baseUrl) {
		this._baseUrl = baseUrl;
	}

	async request(path, method, body, headers) {
        if (body && typeof body === 'object') {
            body = JSON.stringify(body);
        }
		return new Promise((fulfill, reject) => {
			const fullUrl = path.startsWith('http') ? path : `${this._baseUrl}${path}`;
			Request({
				headers: headers,
				uri: fullUrl,
				body: body,
				method: method,
			}, (error, request, response) => {

                if (error) {
					reject(error);
				} else {
                    try {
                        const responseJson = JSON.parse(response);
                        fulfill(responseJson);
                    } catch (e) {
                        reject(new Error(e));
                    }
				}
			});
		});
	}

	async get(data) {
		return await this.request(data.path, 'GET', data.body, data.headers);
	}

	async post(data) {
		return await this.request(data.path, 'POST', data.body, data.headers);
	}

	async put(data) {
		return await this.request(data.path, 'PUT', data.body, data.headers);
	}

	async delete(data) {
		return await this.request(data.path, 'DELETE', data.body, data.headers);
	}

    async uploadPost(data) {
		const  path = data.path;
        const fullUrl = path.startsWith('http') ? path : `${this._baseUrl}${path}`;
        return new Promise((fulfill, reject) => {
            Request.post({url: fullUrl, formData: data.formData, headers: data.headers}, (error, request, response) => {
            	if (error) {
                    reject(error);
                } else {
                    try {
                        const responseJson = JSON.parse(response);
                        fulfill(responseJson);
                    } catch (e) {
                        reject(new Error(e));
                    }
                }
            });
        });

    }

};

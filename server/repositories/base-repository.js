'use strict';

export default class BaseRepository {

    constructor(model) {
        this._model = model
    }

    async getAll(options = {}) {
        if (!options.limit) {
            options.limit = 1000;
        } else {
            options.limit = parseInt(options.limit);
        }
        if (options.page) {
            options.page = parseInt(options.page);
            options.offset = ((options.page - 1) * options.limit);
            delete options.page;
        }
        return await this._model.findAll(options);
    }

    async get(options) {
        return await this._model.findOne(options);
    }

    async create(data, options) {
        return await this._model.create(data, options);
    }

    async update(values, options) {
        return await this._model.update(values, options);
    }

    async destroy(options) {
        options = Object.assign({
            where: {}
        }, options);
        return await this._model.destroy(options);
    }

    async bulkCreate(values) {
        return await this._model.bulkCreate(values);
    }

    async findOrCreate(where, defaults) {
        return await this._model.findOrCreate({where, defaults});
    }

    async count(options = {}) {
        return await this._model.count(options);
    };

    async getAndCountAll(options = {}) {
        return {
            count: await this._model.count({
                where: options.where,
                include: options.include
            }),
            rows: await this.getAll(options)
        };
    }

}
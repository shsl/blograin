'use strict';

import JWT from 'jsonwebtoken';
import {jwtCredentials} from '../config/index';

export default class JWTHelper {

    static getToken(req) {
        let authorization = null;
        let token = null;
        if (req.query && req.query.token) {
            return req.query.token;
        } else if (req.authorization) {
            authorization = req.authorization;
        } else if (req.headers) {
            authorization = req.headers.authorization;
        } else if (req.socket) {
            authorization = req.socket.handshake.headers.authorization;
        }
        if (authorization) {
            const parts = authorization.split(' ');
            if (parts.length === 2) {
                const scheme = parts[0];
                if (/^Bearer$/i.test(scheme)) {
                    token = parts[1];
                }
            }
        }
        return token;
    }

    static async sign(payload, expiresIn = 2592000) {
        return new Promise((fulfill, reject) => {
            JWT.sign(
                payload,
                jwtCredentials.privateKey, {
                    algorithm: 'RS256',
                    expiresIn: expiresIn
                },
                (error, token) => {
                    if (error) {
                        reject(error);
                    } else {
                        fulfill(token);
                    }
                }
            );
        });
    }

    static async verify(token) {
        return new Promise((fulfill, reject) => {
            JWT.verify(
                token,
                jwtCredentials.publicKey,
                {
                    algorithm: 'RS256'
                },
                (error, decoded) => {
                    if (error) {
                        reject(error);
                    } else {
                        fulfill(decoded);
                    }
                }
            );
        });
    }

}
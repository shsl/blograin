'use strict';

import {userRepository} from '../repositories/index.js';
import faker from 'faker';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        try {
            await userRepository.create({
                username: 'letruonglam',
                email: 'lamle@hearti.io',
                password: '$2b$08$Sxpay//2PJMCiGX7UvwFtuM0IFyeRekzI6a3q6WrNwJl0dgaWi2B2'
            });
        } catch (err) {
            console.log(err);
        }
        return true;
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('Person', null, {});
        */
    }
};

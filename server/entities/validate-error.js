'use strict';

export default class ValidateError extends Error {
    constructor(error) {
        super(error);
    }
}
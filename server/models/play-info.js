'use strict';

module.exports = function (sequelize, DataTypes) {
    const PlayInfo = sequelize.define('PlayInfo', {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true
            },
            userId: {
                type: DataTypes.UUID,
            },
            status: {
                type: DataTypes.STRING
            },
            cost: {
                type: DataTypes.DOUBLE
            },
            timeRented: {
                type: DataTypes.DOUBLE
            },
            completeRequest: {
                type: DataTypes.DOUBLE
            },
            rejectRequest: {
                type: DataTypes.DOUBLE
            },
            lastedActivity: {
                type: DataTypes.DATE
            },
            deviceStatus: {
                type: DataTypes.STRING
            },
            language: {
                type: DataTypes.STRING
            },
            averageRating: {
                type: DataTypes.DOUBLE
            }
        },
        {
            paranoid: true,
            freezeTableName: true
        });

    return PlayInfo;
};

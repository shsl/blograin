'use strict';

import {userRepository} from '../repositories/index';
import {Response, HTTPStatus} from '../helpers/index';

export default class UserController {
    getAll = async (req, res) => {
        try {
            const users = await userRepository.getAll({
                order: [
                    ['username', 'asc']
                ]
            });
            return Response.returnSuccess(res, users);
        } catch (error) {
            return Response.returnError(res, error, HTTPStatus.BAD_REQUEST);
        }
    };

    get = async (req, res) => {
        const user = await userRepository.get({
            where: {
                id: req.user.id
            },
            attributes: {
                exclude: ['password']
            }
        });
        if (!user) {
            return Response.returnError(res, new Error('db:'), HTTPStatus.NOT_FOUND);
        }
        return Response.returnSuccess(res, user);
    };

    create = async (req, res) => {
        const body = req.body;
        try {
            const clientNotification = await userRepository.create(body);
            return Response.success(res, clientNotification);
        } catch (e) {
            return Response.error(res, e, HTTPStatus.BAD_REQUEST);
        }
    };

    delete = async (req, res) => {
        try {
            await userRepository.destroy({
                where: {
                    id: req.params.id
                }
            });
            return Response.returnSuccess(res, true);
        } catch (e) {
            return Response.returnError(res, e, HTTPStatus.BAD_REQUEST);
        }
    };

    update = async (req, res) => {
        const body = req.body;
        try {
            const user = await userRepository.update(
                body,
                {
                    where: {
                        id: body.id
                    }
                }
            );
            return Response.returnSuccess(res, user);
        } catch (e) {
            return Response.returnError(res, e, HTTPStatus.BAD_REQUEST);
        }
    };

}
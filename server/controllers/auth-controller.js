'use strict';

import {userRepository} from '../repositories/index';
import {Response, HTTPStatus} from '../helpers/index';
import jwt from 'jsonwebtoken';
import Moment from 'moment';
import ValidationError from '../entities/validate-error';
import {expireTime} from '../config/index';
import JWTHelper from '../helpers/jwt-helper';
import bcrypt from 'bcrypt';

export default class AuthController {

    verifyUser = async (token) => {
        const data = await JWTHelper.verify(token);
        const user = await userRepository.get({
            where: {
                id: data.id
            }
        });
        if (!user) {
            return Promise.reject(new Error('USER_NOT_FOUND'));
        }
        Object.assign(user, data);
        return user;
    };

    isAuth = async (req, res, next) => {
        try {
            if (typeof(req.headers['content-type']) === 'undefined') {
                req.headers['content-type'] = "application/x-www-form-urlencoded";
            }
            const token = JWTHelper.getToken(req);
            if (!token) {
                return Response.error(res, new Error('AUTHORIZATION_FAILED'), HTTPStatus.UNAUTHORIZED);
            }
            const user = await this.verifyUser(token);
            if (user) {
                req.user = user;
                next();
            } else {
                return Response.error(res, new Error('USER_NOT_FOUND'), HTTPStatus.UNAUTHORIZED);
            }
        } catch (e) {
            return Response.error(res, e, HTTPStatus.UNAUTHORIZED);
        }
    };

    create = async (req, res) => {
        const hashedPassword = await bcrypt.hashSync(req.body.password, 8);
        const user = await userRepository.create({
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword
        });

        const token = jwt.sign({id: user.id}, 'supersecret', {
            expiresIn: 86400
        });

        return Response.returnSuccess(res, {
            'access_token': token,
            'token_type': 'Bearer',
            'expires_in': 86400,
            'refresh_token': ''
        });
    };

    getOauthToken = async (req, res) => {
        try {
            const now = Moment().utcOffset(0);
            const {email, password} = req.body;
            if (!this.validateEmail(email)) {
                const error = new ValidationError('Email is not valid');
                return Response.returnError(res, error, HTTPStatus.BAD_REQUEST);
            }
            const user = await userRepository.get({
                where: {
                    email: email
                }
            });
            if (!user) {
                return Response.returnError(res, new ValidationError('User is not found'), HTTPStatus.NOT_FOUND);
            } else if (Moment(user.expiredAt) < now) {
                return Response.returnError(res, new ValidationError('Code has expired to login'), HTTPStatus.BAD_REQUEST);
            } else if (user.validatePassword(password)) {
                const expiredIn = expireTime || 2592000;
                const token = await JWTHelper.sign({
                        id: user.id
                    },
                    expiredIn
                );
                const data = {access_token: token, expired_in: expiredIn};
                return Response.returnSuccess(res, data);
            }
            return Response.returnError(res, new ValidationError('Wrong active password.'), HTTPStatus.BAD_REQUEST);
        } catch (e) {
            return Response.returnError(res, e, HTTPStatus.BAD_REQUEST);
        }
    };

    validateEmail = (email) => {
        if (email.length === 0) return false;
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
        return re.test(email);
    };
}
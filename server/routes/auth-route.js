'use strict';

import {authController} from '../controllers/index';
import {Wrapper} from "../helpers/index";

module.exports = (app, router) => {

    router.route('/register').post([], Wrapper(authController.create));
    router.route('/oauth/token').post([], Wrapper(authController.getOauthToken));
};


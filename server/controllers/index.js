'use strict';

import UserController from './user-controller';
import AuthController from "./auth-controller";

module.exports = {
    userController: new UserController(),
    authController: new AuthController()
};
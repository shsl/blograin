'use strict';

import BCrypt from 'bcrypt';

module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true
            },
            username: {
                type: DataTypes.STRING
            },
            email: {
                type: DataTypes.STRING
            },
            password: {
                type: DataTypes.STRING
            },
            birthDate: {
                type: DataTypes.DATE
            },
            gender: {
                type: DataTypes.STRING
            },
            avatar: {
                type: DataTypes.STRING
            },
            address: {
                type: DataTypes.STRING
            },
            isActive: {
                type: DataTypes.BOOLEAN
            },
            createdAt: {
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                type: DataTypes.DATE,
            }
        },
        {
            paranoid: true,
            freezeTableName: true
        });

    User.associate = (models) => {
        User.hasMany(models.PlayInfo, {
            foreignKey: 'userId',
            as: 'playInfos',
        });
    };

    User.prototype.validatePassword = function (password) {
        return BCrypt.compareSync(password, this.password);
    };

    User.generateHash = async (password) => {
        return await BCrypt.hashSync(password, 8);
    };

    return User;
};
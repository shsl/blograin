'use strict';

module.exports = {
    up: (queryInterface, DataTypes) => {
        return queryInterface.createTable('PlayInfo', {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true
            },
            userId: {
                type: DataTypes.UUID,
            },
            status: {
                type: DataTypes.STRING
            },
            cost: {
                type: DataTypes.DOUBLE
            },
            timeRented: {
                type: DataTypes.DOUBLE
            },
            completeRequest: {
                type: DataTypes.DOUBLE
            },
            rejectRequest: {
                type: DataTypes.DOUBLE
            },
            lastedActivity: {
                type: DataTypes.DATE
            },
            deviceStatus: {
                type: DataTypes.STRING
            },
            language: {
                type: DataTypes.STRING
            },
            averageRating: {
                type: DataTypes.DOUBLE
            }

        });
    },

    down: (queryInterface, DataTypes) => {
        return queryInterface.dropTable('PlayInfo', 'force');
    }
};

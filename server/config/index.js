'use strict';

import DBConfig from './db-config.json';
import Config from './config.json';
import FS from 'fs';
import Path from 'path';

const DotENV = require('dotenv');
DotENV.config();

const env = process.env.NODE_ENV;
const publicKey = FS.readFileSync(Path.resolve(__dirname, 'cert', `${env}.public.key`), 'utf8');
const privateKey = FS.readFileSync(Path.resolve(__dirname, 'cert', `${env}.private.key`), 'utf8');

module.exports = {
    env: env,
    port: process.env.PORT,
    expireTime: process.env.EXPIRE_TIME,
    dbConfig: DBConfig[env],
    config: Config[env],
    resetLifeTime: process.env.RESET_LIFE_TIME,
    jwtCredentials: {
        privateKey: privateKey,
        publicKey: publicKey
    }
};


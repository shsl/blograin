'use strict';

module.exports = {
    up: (queryInterface, DataTypes) => {
        return queryInterface.createTable('User', {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true
            },
            username: {
                type: DataTypes.STRING
            },
            email: {
                type: DataTypes.STRING
            },
            password: {
                type: DataTypes.STRING
            },
            birthDate: {
                type: DataTypes.DATE
            },
            gender: {
                type: DataTypes.STRING
            },
            avatar: {
                type: DataTypes.STRING
            },
            address: {
                type: DataTypes.STRING
            },
            isActive: {
                type: DataTypes.BOOLEAN
            },
            createdAt: {
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                type: DataTypes.DATE,
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('User', 'cascade');
    }
};
